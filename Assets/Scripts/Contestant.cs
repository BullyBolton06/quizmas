﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Contestant : ScriptableObject
{
    public string _name;
    public int _points;

    public void SetUp(string Name)
    {
        _name = Name;
        _points = 0;
    }
}
