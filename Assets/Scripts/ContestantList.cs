﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.Design.Serialization;
using UnityEngine;

[CreateAssetMenu]
public class ContestantList : ScriptableObject
{
    public List<Contestant> Contestants;
    
    public void Init()
    {
        Contestants.Clear();
    }
}
