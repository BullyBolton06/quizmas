﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class QuestionList : ScriptableObject
{
    public List<Question> Questions;
}
