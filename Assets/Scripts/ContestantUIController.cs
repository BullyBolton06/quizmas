﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ContestantUIController : MonoBehaviour
{
    private Contestant _contestantValues;
    [SerializeField] private TextMeshProUGUI _name;
    [SerializeField] private TextMeshProUGUI _points;
    
    public void UpdateStats()
    {
        _name.text = _contestantValues.name;
        _points.text = _contestantValues._points.ToString();
    }

    public void SetContestantVariables(Contestant _contestant)

    {
        _contestantValues = _contestant;
        Debug.Log(_contestantValues._name);
        UpdateStats();
    }
}
