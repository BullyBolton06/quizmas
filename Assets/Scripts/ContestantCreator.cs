﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class ContestantCreator : MonoBehaviour
{
    [SerializeField] private ContestantList _contestantList;
    [SerializeField] private TMP_InputField contestantNameField;
    [SerializeField] private Transform contestantBlock;
    [SerializeField] private GameObject _contestantPrefab;

    private void Awake()
    {
        _contestantList.Init();
    }

    public void CreateContestant()
    {
        if (contestantNameField.text != null)
        {
            Contestant newContestant = ScriptableObject.CreateInstance<Contestant>();
            newContestant.SetUp(contestantNameField.text);
            _contestantList.Contestants.Add(newContestant);
           GameObject contestantUI = Instantiate(_contestantPrefab, contestantBlock);
           contestantUI.GetComponent<ContestantUIController>().SetContestantVariables(newContestant); 
        }
    }

    public void DeleteContestant()
    {
        //_contestantList
    }
    
}
